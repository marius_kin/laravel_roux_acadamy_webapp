<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <link rel="stylesheet" href="{{URL::asset('sass/app.scss')}}" type="text/css"> 
            <div class="content">
                <div class="title">Hello Laravel 5 {{ $theLocation }} on {{ date('Y m d') }} </div>
                @if($theWeather == 'sunny' )
                    <p>it's a beautiful day</p>
                @elseif ($theWeather == 'stormy' )
                    <p>Its stormy</p>
                @else
                    <p>No Forecast available</p>
                @endif
                <p>dont miss</p>
                <ul style="text-align: left">
                    @foreach($theLandmarks as $landmark)
                        @unless($landmark == 'momo')
                            <li>{{ $landmark }}</li>
                        @endunless
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>
