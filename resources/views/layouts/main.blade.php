<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>

        @section('head')
		<script src="{{URL::asset('_scripts/respond.min.js')}}"></script>
        <link href="{{URL::asset('_css/main.css')}}" rel="stylesheet" media="screen, projection">
        @show
        
    </head>
    <body>
        @yield('header')
    	@yield('body')
    </body>
</html>
