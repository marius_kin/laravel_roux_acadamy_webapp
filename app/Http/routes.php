<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Controller@showAbout');

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('about/directions', array( 'as' => 'directions' , function(){
    $theURL = URL::route('directions');
    return "DIRECTION goes to this URL: $theURL";
}));

Route::get('about', function(){
    return 'm content';
});

Route::any('submit-form', function (){
   return 'sth'; 
});

Route::get('about/{theSubject}', function($theSubject){
    return $theSubject . ' content goes here!';
});

Route::get('about/classes/{theSubject}', function($theSubject){
    return $theSubject . ' content goes here!';
});

Route::get('programs', function(){
	return View::make('programs');
});

Route::get('graphic-design', function(){
	return View::make('graphic-design');
});

Route::get('signup', function(){
	return View::make('signup');
});

Route::get('signup', function(){
	return View::make('signup');
});

Route::post('thanks', function(){
	$theEmail = Request::input('email');
	return View::make('thanks')->with('theEmail', $theEmail);
});