<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use \App\Models\Paintings;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    

   
    
public function showWelcome(){

	 // Schema::create('art', function($newTable)
    // {
    // 	// 	$newTable->increments('id');
    // 	// $newTable->string('artist');
    // 	// $newTable->string('title', 500);
    // 	// $newTable->text('description');
    // 	// $newTable->date('created');
    // 	// $newTable->date('exhibition_date');
    // 	// $newTable->timestamps();
    
    // });

    // Schema::table('art', function($newcolumn){
    // 	$newcolumn->boolean('alumni');
    // 	$newcolumn->dropColumn('exhibition_date');
    // });

    // Schema::dropIfExists('art');

    // Schema::create('sculpture', function($table){
    // 	$table->increments('id');
    // 	$table->string('artist');
    // 	$table->string('title', 500);
    // 	$table->date('created');
    // 	$table->timestamps();
    // });

    // Schema::table('sculpture', function($newcolumn){
    // 	$newcolumn->text('description');
    // });

    // Schema::dropIfExists('sculpture');





    $theLandmarks = array('momo', 'dodo', 'yolo');
    return view('welcome', array('theLocation' => 'NYC', 'theWeather' => FALSE, 'theLandmarks'=> $theLandmarks));
}

public function showAbout(){

	$painting = new Paintings;
    $painting->title = 'Do no wrong';
    $painting->artist = 'D. Doright';
    $painting->year = 2014;
    $painting->save();


    return view('about');
}

}
