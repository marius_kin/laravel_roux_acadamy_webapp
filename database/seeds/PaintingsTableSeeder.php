<?php

use Illuminate\Database\Seeder;
use \App\Models\Paintings;

class PaintingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

		Paintings::truncate();

		for ($i = 0; $i < 50;  $i++ ){

			$painting = Paintings::create(array(
					'title' => $faker->realText(rand(20,40)),
					'artist' => $faker->name,
					// 'graduate' => rand(1,0),
					'year' => $faker->year
				));
		}
    }
}
